import axios from 'axios';
import React, { Component } from 'react';
import Advertisement from '../Layout/Advertisement';
import Leftsidebar from '../Layout/Left-sidebar'
import Product_list from './Product_list';
export default class index extends Component {
 
    render() {
        return (
            <>
               <Advertisement/>
               <section>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-3">
                                <Leftsidebar/>
                            </div>
                            
                            <div class="col-sm-9 padding-right">
                               <Product_list/>
                            </div>
                        </div>
                    </div>
                </section>
            </>
        )
    }
}
