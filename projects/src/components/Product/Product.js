import axios from 'axios';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Addcart from '../Cart/Addcart'
import AddWishlist from '../Wishlist/AddWishlist';
export default class Product extends Component {
    constructor(props){
        super(props);
        this.state={
            data:[]
        }
    }
    componentDidMount(){
        axios.get('http://localhost/laravel/public/api/product')
        .then(res=>{
            this.setState({
                data: res.data.data
            })
        })
        .catch(err=>console.log(err))
    }
    renderData(){
        let {data} = this.state;
        if(data.length > 0){
            return data.map((value)=>{
                let img = JSON.parse(value['image'])
                return(
                    <div class="col-sm-4">
                        <div class="product-image-wrapper">
                            <div class="single-products">
                                    <div class="productinfo text-center">
                                        <img src={'http://localhost/laravel/public/upload/user/product/'+value['id_user']+'/'+img[0]} alt="" />
                                        <h2>${value['price']}</h2>
                                        <p>{value['name']}</p>
                                        <Addcart id={value['id']} />
                                    </div>
                                    <div class="product-overlay">
                                        <div class="overlay-content">
                                            <h2>${value['price']}</h2>
                                            <Link to={'/product/details/'+value['id']}>
                                            <p>{value['name']}</p>
                                            </Link>
                                            <Addcart id={value['id']} />
                                        </div>
                                    </div>
                            </div>
                            <div class="choose">
                                <ul class="nav nav-pills nav-justified">
                                    <li><AddWishlist id={value['id']}/></li>
                                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                )
            })
        }
    }
    render() {
        return (
            <div class="features_items">
                <h2 class="title text-center">Features Items</h2>
                {this.renderData()}
            </div>
        )
    }
}
