import React, { Component } from 'react';
import { AppContext } from '../contexts/AppContext';
import {withRouter} from 'react-router-dom';

const    IDwishlist=[];
class AddWishlist extends Component {
    static contextType = AppContext;
    constructor(props){
        super(props);
        this.state={
            id: props.id,
        }
        this.handleIDwishlist=this.handleIDwishlist.bind(this)
    }
    handleIDwishlist(){
        if(localStorage.getItem('IDwishlist')){
            let IDwishlist = JSON.parse(localStorage.getItem('IDwishlist'));
            if(IDwishlist.includes(this.state.id)){
                alert("Sản phẩm đã được thêm vào mục ưa thích")
            }else{
                IDwishlist.push(this.state.id)
                let qtyWishlist = IDwishlist.length
                this.context.qtyWishlist(qtyWishlist);
                localStorage.setItem('IDwishlist',JSON.stringify(IDwishlist))
            }
        }else{
            if(IDwishlist.includes(this.state.id)){
                alert("Sản phẩm đã được thêm vào mục ưa thích")
            }else{
                IDwishlist.push(this.state.id)
                let qtyWishlist = IDwishlist.length
                this.context.qtyWishlist(qtyWishlist);
                localStorage.setItem('IDwishlist',JSON.stringify(IDwishlist))
            }
        }
    }
    render() {
        return (
            <>
                <a onClick={this.handleIDwishlist}><i class="fa fa-plus-square"></i>Add to wishlist</a>
            </>
        )
    }
}
export default  withRouter(AddWishlist);