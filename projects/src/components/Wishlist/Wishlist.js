import axios from 'axios';
import React, { Component } from 'react'
import { AppContext } from '../contexts/AppContext';

export default class Wishlist extends Component {
    static contextType = AppContext;
    constructor(props){
        super(props);
        this.state={
            data:[],
            IDwishlist:null
        }
        this.handleDelete=this.handleDelete.bind(this)
    }
    componentDidMount(){
       let getDataID =  localStorage.getItem('IDwishlist');
       getDataID = JSON.parse(getDataID);
       axios.get('http://localhost/laravel/public/api/product/wishlist')
       .then(res=>{
           this.setState({
               data: res.data.data,
               IDwishlist:getDataID
           })
       })
       .catch(err=>console.log(err))
    }
    handleDelete(e){
        let id = parseInt(e.target.id);
        let IDwishlist= new Array();
        IDwishlist = this.state.IDwishlist;
        let newIDwishlist = IDwishlist.filter(item => item !== id)
        this.setState({
            IDwishlist: newIDwishlist
        })
        console.log(newIDwishlist.length)
        let qtyWishlist = newIDwishlist.length;
        this.context.qtyWishlist(qtyWishlist)
        localStorage.setItem('IDwishlist', JSON.stringify(newIDwishlist) )
    }
    renderWishlist(){
        if(localStorage.getItem('IDwishlist')){
        let IDwishlit = new Array();
            IDwishlit = this.state.IDwishlist;
        let data = this.state.data;
        return data.map((value,key)=>{
            let imglist = new Array();
            imglist = value['image'];
            let replace1 = imglist.replace(']','');
            let replace2 = replace1.replace('[','');
            let img = JSON.parse("["+replace2+"]");
            if(IDwishlit.includes(value.id)){

                return(
                    <tr>
                        <td class="cart_product">
                            <a href=""><img src={'http://localhost/laravel/public/upload/user/product/'+value['id_user']+'/'+img[0]} width='100' alt=""/></a>
                        </td>
                        <td class="cart_description">
                            <h4><a href="">{value['name']}</a></h4>
                            <p>Web ID: 1089772</p>
                        </td>
                        <td class="cart_price">
                            <p>{value['detail']}</p>
                        </td>
                        <td class="cart_price">
                            <p>${value['price']}</p>
                        </td>
                        <td class="cart_delete">
                            <a class="cart_quantity_delete" ><i class="fa fa-times" id={value.id} onClick={this.handleDelete}></i></a>
                        </td>
                    </tr>
                )
            }
        })
        }
    }
    render() {
        return (
            <>
             <section id="cart_items">
                    <div class="container">
                        <div class="breadcrumbs">
                            <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li class="active">Wishlist</li>
                            </ol>
                        </div>
                        <div class="table-responsive cart_info">
                            <table class="table table-condensed">
                                <thead>
                                    <tr class="cart_menu">
                                        <td class="image">Item</td>
                                        <td class="description"></td>
                                        <td class="price">Details</td>
                                        <td class="price">Price</td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.renderWishlist()}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>   

                
            </>
        )
    }
}
